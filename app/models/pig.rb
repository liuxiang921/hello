class Pig
  include Mongoid::Document
  field :name, type: String
  field :age, type: String
  field :gender, type: String
end
