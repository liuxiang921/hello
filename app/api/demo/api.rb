module Demo
  class API < Grape::API

    prefix :api
    version 'v1', :using => :path
    format :json

    error_formatter :json, ->(message, backtrace, options, env) {
      "error"
    }

    rescue_from :all

    resource :posts do
      get 'first' do
        present Post.first
      end
    end
  end

end
